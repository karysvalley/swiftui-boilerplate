//
//  itemDetailsCompleteView.swift
//  CodeDesignStudies
//
//  Created by johnnie fujita on 24/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct itemDetailsCompleteView: View {
    var item: ItemDetails
    @Binding var active: Bool
    @Binding var activeIdx: Int
    @Binding var show: Bool
    
    
    var body: some View {
         ScrollView {
            VStack {
                VStack {
                    HStack(alignment: .top) {
                        VStack(alignment: .leading, spacing: 8.0) {
                            Text(item.title)
                                .font(.system(size: 24, weight: .bold)).foregroundColor(Color(item.colorText))
                                Text(item.subtitle).foregroundColor(Color.white.opacity(0.7))
                        }
                        Spacer()
                        
                        ZStack {
                            VStack {
                                Image(systemName: "xmark")
                                            .foregroundColor(.white)
                                            .font(.system(size: 16, weight: .semibold))
                                           
                                    }
                                .onTapGesture {
                                self.show = false
                                   self.active = false
                                self.activeIdx = -1
                                }
                                .frame(width: 36, height: 36)
                                .background(Color.black)
                                .clipShape(Circle())
                            }
                        
                            
                    }
                    Spacer()
                    WebImage(url: item.image)
                        .resizable()
                        .scaledToFit()
                        .frame(maxWidth: .infinity)
                        .frame(height: 140, alignment: .top)
                }
                .padding(show  ? 30 : 20)
                .padding(.top, show  ? 30 : 0)
                .frame(maxWidth: show  ? .infinity : screen.width - 60, maxHeight: show  ? screen.height / 2 : 280)
                .background(Color(item.color))
                .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
                .shadow(color: Color(item.color).opacity(0.3), radius: 10, x: 0, y: 10)
                
                VStack(alignment: .leading, spacing: 30.0) {
                    Text("Take your SwiftUI app to the app Store with advanced techiniques like API data, packages and CMS")
                    
                    Text("About this App.")
                        .font(.title)
                        .bold()
                    
                    Text("I Thought i Would live just for 100 years, but being alive for 500 years has  been a pleasure, i`ve met so many incredible humans. The hardest part was to keep the memory of the ones from the start. The human memory is not the great, therefore, many of the old memories and persons from the early days, simply vanished, sometimes i look at pictures from that time - pictures that are very rare for me - and i just can`t believe that i`ve been to some places, some eras, and with some people that clearly were very intimate to me.")
                    
                    Text("I Thought i Would live just for 100 years, but being alive for 500 years has  been a pleasure, i`ve met so many incredible humans. The hardest part was to keep the memory of the ones from the start. The human memory is not the great, therefore, many of the old memories and persons from the early days, simply vanished, sometimes i look at pictures from that time - pictures that are very rare for me - and i just can`t believe that i`ve been to some places, some eras, and with some people that clearly were very intimate to me.")
                }
                .padding(30)
            }
        }
            
        .edgesIgnoringSafeArea(.all)
    }
}

struct itemDetailsCompleteView_Previews: PreviewProvider {
    static var previews: some View {
        itemDetailsCompleteView(item: itemAsPlatData[0], active: .constant(true), activeIdx: .constant(-1), show: .constant(true))
    }
}

