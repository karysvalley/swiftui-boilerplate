//
//  UpdateList.swift
//  CodeDesignStudies
//
//  Created by johnnie fujita on 20/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI

struct UpdateList: View {
    @ObservedObject var store = UpdateStore()
    
    func addUpdate() {
        store.updates.append(Update(image: "Card1", title: "New Item", text: "Text", date: "Jun 3"))
    }
    
    var body: some View {
        NavigationView {
            List {
                ForEach(store.updates) { update in
                NavigationLink(destination: UpdateView(update: update)) {
                    HStack {
                        Image(update.image)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 80, height: 80)
                            .background(Color.black)
                            .cornerRadius(20)
                            .padding(.trailing, 4)
                        
                        VStack(alignment: .leading) {
                            Text(update.title)
                                .font(.system(size: 20, weight: .bold))
                                    
                            Text(update.text)
                                .lineLimit(2)
                                .font(.subheadline)
                            .foregroundColor(Color(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)))
                            
                            Text(update.date)
                                .font(.caption)
                                .foregroundColor(.secondary)
                                    .fontWeight(.semibold)
                        }
                    }
                    .padding(.vertical, 8)
                }
            }
                .onDelete { index in
                    self.store.updates.remove(at: index.first!)
                }
                .onMove { (source: IndexSet, destination: Int) in
                self.store.updates.move(fromOffsets: source, toOffset: destination)
                }
        }
        .navigationBarTitle(Text("Updates"))
            .navigationBarItems(leading: Button(action: {self.addUpdate()}) {
                Image(systemName: "plus")
            }, trailing: EditButton())
        
        }
    }
}

struct UpdateList_Previews: PreviewProvider {
    static var previews: some View {
        UpdateList()
    }
}


let updateList: [Update] = [
    Update(image: "Card1", title: "Microappollis", text: "Join the firt Virtual Democratic Capital", date: "Jan 1"),
    Update(image: "Card2", title: "Virtual Democracies", text: "The User`s Owned Platform", date: "JUL 21"),
    Update(image: "Card3", title: "Olkeey", text: "Walletless Events Experience and Management", date: "Jan 12"),
    Update(image: "Card4", title: "iFood", text: "Imagine your life without", date: "Jan 29")
]

