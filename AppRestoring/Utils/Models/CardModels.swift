//
//  CardModels.swift
//  AppRestoring
//
//  Created by johnnie fujita on 04/03/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI

struct ItemDetails: Identifiable {
    var id = UUID()
    var title: String
    var subtitle: String
    var image: URL
    var logo: UIImage
    var color: UIColor
    var colorText: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    var show: Bool
}


struct Post: Codable, Identifiable {
    let id = UUID()
    var title: String
    var body: String
}


struct Update: Identifiable {
    var id = UUID()
    var image: String
    var title: String
    var text: String
    var date: String
    
}
